import "./MainPage.css";

function MainPage() {
  return (
    <div
      className="px-4 py-5 my-5 text-center text-black bg-light bg-opacity-50"
      id="MainTitle"
    >
      <h1 className="display-1 fw-bold" id="Kai">
        KAI'S CARS
      </h1>
      <div className="col-lg-6 mx-auto" id="MainPhase">
        <p>Provided By</p>
        <p className="lead mb-4">
          <button
            type="button"
            className="btn btn-danger"
            data-bs-toggle="modal"
            data-bs-target="#myModal"
          >
            Sponsor
          </button>
        </p>
        <div className="modal fade" id="myModal">
          <div className="modal-dialog">
            <div className="modal-content">
              <h2>
                <p>WARREN'S WHEELS</p>
                <p>Service Department</p>
              </h2>
            </div>
          </div>
          <div className="modal-header">
            <h4 className="modal-title">Modal Heading</h4>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
            ></button>
          </div>
          <div className="modal-body text-black bg-white modal-dialog modal-lg">
            FEATURING THE HIGH QUALITY PARTS FURNISHED AND PRODUCED BY WARREN'S
            WHEELS OF PHILADELPHIA, PA
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-danger"
              data-bs-dismiss="modal"
            >
              Close
            </button>
          </div>
        </div>

        <div className="offcanvas offcanvas-start" id="demo">
          <div className="offcanvas-header">
            <h1 className="offcanvas-title">Follow Us</h1>
            <button
              type="button"
              className="btn-close text-reset"
              data-bs-dismiss="offcanvas"
            ></button>
          </div>
          <div className="offcanvas-body">
            <p>
              <br /> <br /> <br /> <br /> <br />
              <br /> <br /> <br /> <br /> <br />
              <a href="https://www.linkedin.com/in/dean-grey-739377220/">
                Dean's LinkedIn
              </a>
            </p>
            <br /> <br />
            <p>
              <a href="https://www.linkedin.com/in/jeremiah-rigby/">
                Jeremiah's LinkedIn
              </a>
            </p>
            <br /> <br />
            <a
              href="https://gitlab.com/OmegaDeanGrey/2nd-project-beta"
              className="btn btn-outline-danger"
              role="button"
            >
              Git Repo
            </a>
          </div>

          <div className="carousel-inner">
            <div className="carousel-item active">
              <img
                src="https://www.chevrolet.com/content/dam/chevrolet/na/us/english/vdc-collections/2023/perfomance/corvette/01-images/nav/2023-corvette-3lt-gkz-driver-front-3quarter-nav.jpg?imwidth=960"
                alt="Los Angeles"
                class="d-block w-100"
              />
            </div>
            <div className="carousel-item">
              <img
                src="https://www.bugatti.com/fileadmin/_processed_/e/e/csm_og-image_506cf6a92e.jpg"
                alt="Chicago"
                class="d-block w-100"
              />
            </div>
            <div className="carousel-item">
              <img
                src="https://mclaren.scene7.com/is/image/mclaren/McLarenArtura-16:crop-16x9?wid=1980&hei=1114"
                alt="New York"
                class="d-block w-100"
              />
              <button
                class="carousel-control-prev"
                type="button"
                data-bs-target="#demo1"
                data-bs-slide="prev"
              >
                <span class="carousel-control-prev-icon"></span>
              </button>
              <button
                class="carousel-control-next"
                type="button"
                data-bs-target="#demo2"
                data-bs-slide="next"
              >
                <span class="carousel-control-next-icon"></span>
              </button>
            </div>
          </div>
        </div>

        <button
          class="btn btn-danger"
          type="button"
          data-bs-toggle="offcanvas"
          data-bs-target="#demo"
        >
          Follow Us
        </button>
      </div>
    </div>
  );
}
export default MainPage;
