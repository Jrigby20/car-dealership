import React, { useState, useEffect } from "react";



function InventoryList() {
    const [autos, setAutos] = useState([])


    const getAutoData = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles/")
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    }

    useEffect(() => {
        getAutoData()
    }, []
    )

    return (
        <div>
            <h1 className="text-light">
                Inventory
            </h1>
            <table className="table table-danger">
                <thead>
                    <tr>
                        <th>Color</th>
                        <th>Year</th>
                        <th>VIN</th>
                        <th>Model</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => {
                        return (
                            <tr key={auto.id}>
                                <td> {auto.color}</td>
                                <td> {auto.year}</td>
                                <td> {auto.vin} </td>
                                <td> {auto.model.name} </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>

    )
}
export default InventoryList;
