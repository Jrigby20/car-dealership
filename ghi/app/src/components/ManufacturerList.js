import React, { useState, useEffect } from "react";



function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([])

    
    const getManufacturerData = async() => {
        const response = await fetch("http://localhost:8100/api/manufacturers/")
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(()=>{
      getManufacturerData()
    },[]
    )

    return (
         <div>
            <h1 className="text-light">
                Manufacturers
            </h1>
            <table className="table table-danger">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                    return(
                    <tr key={manufacturer.id}>
                        <td> { manufacturer.name }</td>
                    </tr>
                    ) })}
                </tbody>
            </table>
        </div>               
    )}
export default ManufacturerList;