import React, { useState, useEffect } from "react";


function SalesCustomerList() {
    const [customers, setCustomers] = useState([])

    
    const getCustomerData = async() => {
        const response = await fetch("http://localhost:8090/api/customers/")
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    useEffect(()=>{
      getCustomerData()
    },[]
    )

    return (

        <div>
            <h1 className="text-light">
                Customers
            </h1>
            <table className="table table-danger">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                    return(
                    <tr key={customer.id}>
                        <td> { customer.name }</td>
                        <td> {customer.address}</td>
                        <td> {customer.phone_number} </td>
                    </tr>
                    ) })}
                </tbody>
            </table>
        </div>            
    )}
export default SalesCustomerList;