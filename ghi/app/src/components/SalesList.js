import { useState, useEffect } from "react";


function SalesList() {

    const [salesData, setSalesData] = useState([]);
    const [salesPersons, setSalesPersons] = useState([]);
    const [afterFilterLists, setAfterFilterLists] = useState([])
    const noData = []


    const getSalesPersons = async () => {
        const salesPersonsUrl = "http://localhost:8090/api/salespersons/";
        const response = await fetch(salesPersonsUrl);
        if (response.ok) {
            const data = await response.json();
            setSalesPersons(data.salespersons);
        } else {
            alert("Something went wrong");
        }
    }

    const getSales = async () => {
        const salesUrl = "http://localhost:8090/api/salesforms/";
        const response = await fetch(salesUrl);
        if (response.ok) {
            const data = await response.json();
            setSalesData(data.salesform);
            return data.salesform;
        } else {
            alert("Something went wrong");
        }
    }

    const handleChange = async (event) => {
        await getSales();
        const filteredList = salesData.filter(sale => sale.salesperson.name === event.target.value);
        setAfterFilterLists(filteredList);
    }


    const handleReset = async (event) => {
        noData()
    }

    useEffect(() => {
        getSalesPersons();
        getSales();
    }, []);

    return (
        <div>
            <br />
            <h1 className="text-white">Sales History</h1>
            <div className="mb-3">
                <select onChange={handleChange} required id="id" name="salesperson" className="form-select">
                    <option value="">Select Salesperson to View Record</option>
                    {salesPersons.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.id}>
                                {salesperson.name}
                            </option>
                        )
                    })}

                </select>
                <button onClick={handleReset} className="form-reset">
                </button>
                <br />
                <br />
                <h2>
                    <table className="table table-striped text-light bg-danger bg-opacity-75">
                        <thead>
                            <tr>
                                <th>SALESPERSON</th>
                                <th>EMPLOYEE ID</th>
                                <th>PURCHASER</th>
                                <th>VIN</th>
                                <th>SALE PRICE</th>
                            </tr>
                        </thead>
                        <tbody>
                            {afterFilterLists.map(saleData => {
                                return (
                                    <tr key={saleData.id}>
                                        <td>{saleData.salesperson.name}</td>
                                        <td>{saleData.salesperson.employee_number}</td>
                                        <td>{saleData.customer.name}</td>
                                        <td>{saleData.automobile.vin}</td>
                                        <td>{saleData.price}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </h2>
            </div>
        </div>
    )
}
export default SalesList;
