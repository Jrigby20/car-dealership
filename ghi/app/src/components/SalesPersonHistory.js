import React, { useState, useEffect } from "react";


function SalesPersonHistory() {
    const [salespersons, setSalespersons] = useState([])


    const getSalesPersonData = async() => {
        const response = await fetch("http://localhost:8090/api/salespersons/")
        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.salespersons);
        } 
}

    useEffect(()=>{
        getSalesPersonData()
    },[]
    )

    return( 
        <div>
            <h1 className="text-light">
                SalesPeople
            </h1>
            <table className="table table-danger">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Employee Number</th>
                    </tr>
                </thead>
                <tbody>
                    {salespersons.map(salesperson => {
                    return(
                    <tr key={salesperson.id}>
                        <td> { salesperson.name }</td>
                        <td> {salesperson.employee_number}</td>
                    </tr>
                    ) })}
                </tbody>
            </table>
        </div>
    )}
export default SalesPersonHistory;

