import React, { useState } from 'react'

export default function ServiceTechnicianForm() {
    const [techincianName, setTechnicianName] = useState("")
    const [employeeId, setEmployeeId] = useState("")


    const handleTechnicianNameChange = (e) => {
        setTechnicianName(e.target.value)
    }

    const handleEmployeeIdChange = (e) => {
        setEmployeeId(e.target.value)
    }


    const handleSubmit = async (e) => {
        e.preventDefault()
        const data = {}
        data.name = techincianName
        data.employee_number = employeeId

        const techUrl = "http://localhost:8080/api/technicians/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const techResponse = await fetch(techUrl, fetchConfig)
        if (techResponse.ok) {
            setTechnicianName("")
            setEmployeeId("")
        }
    }



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4 bg-danger">
                    <h1 className="text-white">Add New Technician</h1>
                    <form onSubmit={handleSubmit} id="appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleTechnicianNameChange} value={techincianName} placeholder="Enter Technician Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIdChange} value={employeeId} placeholder="Enter Employee ID" required type="number" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-light">Add Technician</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
