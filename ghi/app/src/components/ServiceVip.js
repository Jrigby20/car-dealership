import React from 'react'

export default function ServiceVip(props) {
    if (props.vip === false) {
        return (
            <div>No</div>
        )
    }
    else {
        return (
            <div>Yes!</div>
        )
    }
}
