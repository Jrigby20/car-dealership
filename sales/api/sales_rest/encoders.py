from common.json import ModelEncoder
from .models import Customer, SalesPerson, AutomobileVO, SalesForm


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


class SalesFormEncoder(ModelEncoder):
    model = SalesForm
    properties = [
        "price",
        "customer",
        "salesperson",
        "automobile",
    ]
    encoders = {
        "customer" : CustomerEncoder(),
        "salesperson" : SalesPersonEncoder(),
        "automobile" : AutomobileVOEncoder(),
    }