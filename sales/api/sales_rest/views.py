import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import (
    Customer,
    SalesPerson,
    AutomobileVO,
    SalesForm,
)
from .encoders import (
    CustomerEncoder,
    SalesPersonEncoder,
    SalesFormEncoder,
)

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Customer already exists"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_customer(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        Customer.objects.filter(id=id).update(**content)

        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_salespersons(request):
    if request.method == "GET":
        salespersons = SalesPerson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Employee already exists"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_salesperson(request, id):
    if request.method == "GET":
        salesperson = SalesPerson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = SalesPerson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        SalesPerson.objects.filter(id=id).update(**content)

        salesperson = SalesPerson.objects.get(employee_number=id)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_saleslist(request, salesperson_id=None):
    if request.method == "GET":
        if salesperson_id is not None:
            salesform = SalesForm.objects.filter(salesperson_id=salesperson_id)
        else:
            salesform = SalesForm.objects.all()
        return JsonResponse(
            {"salesform": salesform},
            encoder=SalesFormEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            sales_name = content["salesperson"]
            salesperson = SalesPerson.objects.get(name=sales_name)
            content["salesperson"] = salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson Error"},
                status=404,
            )
        try:
            customer_name = content["customer"]
            customer = Customer.objects.get(name=customer_name)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer Error"},
                status=404,
            )
        try:
            vin = content["automobile"]
            auto = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = auto
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automotive Error"},
                status=404,
            )
        salesform = SalesForm.objects.create(**content)
        return JsonResponse(
            salesform,
            encoder=SalesFormEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_salesform(request, id):
    if request.method == "GET":
        sale = SalesForm.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SalesFormEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = SalesForm.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        SalesForm.objects.filter(id=id).update(**content)

        sale = SalesForm.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SalesFormEncoder,
            safe=False,
        )
