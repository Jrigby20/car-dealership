from common.json import ModelEncoder
from .models import Appointment, Technician

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number"
    ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "reason",
        "vip",
        "appointment_status",
        "technician"
        ]

    encoders = {
        "technician": TechnicianEncoder(),
    }

    def get_extra_data(self, o):
        if type(o.date) == str:
            return{
                "date": o.date,
                "time": o.time
            }
        return {
            "date": o.date.strftime("%b-%d, %Y"),
            "time": o.time.strftime("%H:%M")
        }
