from django.db import models

# Create your models here.
class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.SmallIntegerField()
    def __str__(self):
        return self.name

class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=50)
    date = models.DateField()
    time = models.TimeField()
    reason = models.TextField(max_length=1000)
    vip = models.BooleanField(default=False)
    appointment_status = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE
    )
    def __str__(self):
        return self.vin
