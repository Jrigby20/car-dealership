from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .encoders import AppointmentListEncoder, TechnicianEncoder
import json
from django.http import JsonResponse
from .models import Appointment, Technician


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(name=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician Name does not exist"},
                status=404
            )
        appoitment = Appointment.objects.create(**content)
        return JsonResponse(
            appoitment,
            encoder=AppointmentListEncoder,
            safe=False
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_appointment(request, id):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT":
        content =json.loads(request.body)
        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )




@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
            )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_show_technicians(request, id):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
