import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
# from service_rest.models import Something
from service_rest.models import Appointment


def get_vins():
    list_vins = []
    response = requests.get('http://inventory-api:8000/api/automobiles/')
    content = json.loads(response.content )
    for car in content["autos"]:
        list_vins.append(car["vin"])
    appointments = Appointment.objects.all()
    for appointment in appointments:
        if appointment.vin in list_vins:
            Appointment.objects.filter(vin= appointment.vin).update(vip=True)



def poll():
    while True:
        print('Service poller polling for data')
        try:
            get_vins()
            print("Poller Functioning Correctly!")
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)


if __name__ == "__main__":
    poll()
